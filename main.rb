class LookAndSay
    attr_reader :value, :count

    def initialize(value)
        @value = value
        @count = 1
    end

    def add
        @count += 1
    end

    def to_s
        "#{@count.to_s}#{@value.to_s}"
    end
end

def look_and_say(string)
    result = []
    a = string.split('')
    current = nil
    0.upto(a.length - 1) do |i|
        if current.nil? || current.value != a[i]
            result << current unless current.nil?
            current = LookAndSay.new(a[i])
        else
            current.add
        end
    end
    result << current unless current.nil?
    result.flatten.join
end

def look_and_say_sequence(first_element, n)
    result = first_element
    (n - 1).times do
        result = look_and_say(result)
    end
    result
end

puts look_and_say_sequence("1", 1) #   == "1"
puts look_and_say_sequence("1", 3) #   == "21"
puts look_and_say_sequence("1", 5) #   == "111221"
puts look_and_say_sequence("22", 10) # == "22"
puts look_and_say_sequence("14", 2) #  == "1114"